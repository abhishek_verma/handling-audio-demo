package com.jombay.audioplaybackdemo;

import android.app.Activity;
import android.media.MediaPlayer;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

public class AudioActivity extends Activity {
    private View mView;
    private Button playpause;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audio);

        playpause = (Button) findViewById(R.id.playpause);


        final MediaPlayer mp = MediaPlayer.create(AudioActivity.this, R.raw.twist_shout);

        playpause.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (mp.isPlaying()) {
                    mp.pause();
                    playpause.setText("Play");
                    Toast.makeText(AudioActivity.this, "Play", Toast.LENGTH_SHORT).show();
                } else {
                    mp.start();
                    playpause.setText("Stop");
                    Toast.makeText(AudioActivity.this, "Stopped", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
